package test.testng.sauce;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.annotations.TestInstance;



import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

public class testNGwithSauceLab {
	
	@FindBy(css="input[id='lst-ib']")
	private WebElement searchBox;
	WebDriver driver;
	
	@FindBy(css="input[value='Google Search']")
	private WebElement searchButton;
	
	@DataProvider(name="datademo")
	public Object[][] saucelabData() {
		 return new Object[][] {
		   { "demo1", "provider1" },
		   { "demo2", "provider2" },
		   { "demo3", "provider3" },
		 };
		}
	
/*
 	public static final String USERNAME = "sindhug108";
	DesiredCapabilities caps;
	public static final String ACCESS_KEY = "5d84a5ff-a8c9-4882-aada-611fb7425358";
	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
	  */
	@Parameters("browser")
	@BeforeClass
	public void doSetup(String browser)
	{
		
		 if(browser.equals("chrome"))
		  {
		/*    caps = DesiredCapabilities.chrome();
		    caps.setCapability("platform", "Windows 10");
		    caps.setCapability("version", "43.0");*/
			  System.setProperty("webdriver.chrome.driver","src/test/resources/chromedriver.exe");
				driver = new ChromeDriver();
		  }
		  if(browser.equals("firefox"))
		  {
		/*    caps = DesiredCapabilities.firefox();
		    caps.setCapability("platform", "Windows 7");
		    caps.setCapability("version", "55.0");*/
			  System.setProperty("webdriver.gecko.driver","src/test/resources/geckodriver.exe");
				driver= new FirefoxDriver();
		  }
	}
	
  
  @Test(priority=1)
  public void sauceLabBrowserSelection() throws Exception {
	 
	    driver.get("https://www.google.com");
	    assertEquals(driver.getTitle(),"Google");
  }
	  
	  @Test(priority=2)
	  public void selectShop() throws Exception
	  {
		  PageFactory.initElements(driver,this); 		  
		  searchBox.click();
		  searchBox.sendKeys("capgemini");
		  searchButton.click();
		  assertEquals(driver.getTitle().contains("Google"),true);
	  }
	  
	  @Test(priority=3,dataProvider="datademo")
	  public void printDataFromDataProvider(String name1, String name2)
	  {
		  System.out.println("*** "+name1+" : "+name2+" ***");
	  }

}
